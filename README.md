# Kotlin in 30 minutes

Some Kotlin highlights (not only, but mainly) for Java developers.

The whole demo is in one file named `KtIn30mins.kt` in the `src/main/kotlin` directory.

If you got interested in Kotlin there is another, much more in-depth demo in the same project and same directory 
named `KtIn120mins.kt`. 
