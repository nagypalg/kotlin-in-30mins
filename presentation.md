---
marp: true

---

<!-- 
$theme: gaia
template: invert
footer: Kotlin in 30 mins
-->

<!-- $size: 16:9 -->
<!-- page_number: true -->


Kotlin in 30 minutes
===

# ![Kotlin Logo 10%](logo.png)

##### (not only) for Java developers

###### Gabor Nagypal

<!-- 
*page_number: false 
*footer:
-->

---

# Very brief history of Kotlin

- First OSS version in 2012 
- Created by JetBrains (creator of IntelliJ)
- Name: island near to St. Petersburg
- Version 1.0 in 2016
- Current version: 1.3, released late 2018
- May 7., 2019, Google: **preferred** language for Android

---

# What Kotlin used for

- Android development (Java 6 compatible)
- Backend development (main domain of Java)
  - Kotlin is 100% compatible with Java (both ways!)
- Recent options: Kotlin for JS, Kotlin Native

---

# Why Kotlin (and not Java)?

- Same level of IDE support (IntelliJ)
- More concise and readable, but at the same time also safer
  - Functions are first class citizens, Properties, Data classes, Extension functions, Better type system (no NPE) and generics, Smart casts, Coroutines, ...
- Easier to upgrade (just upgrade a JAR vs. the whole JVM)
- See also https://kotlinlang.org/docs/reference/comparison-to-java.html

---

# Code walkthrough

- https://gitlab.com/nagypalg/kotlin-in-30mins
- KtIn30mins.kt

---

# What's next?

- Check out KtIn120mins.kt
- [Kotlin reference guide](https://kotlinlang.org/docs/reference/)
- [Awesome Kotlin](http://kotlin.link/)
- [Kotlin by Example](https://play.kotlinlang.org/byExample/overview)
- [Kotlin Koans](https://play.kotlinlang.org/koans/overview)

---

## Thank you for your time! :smiley: