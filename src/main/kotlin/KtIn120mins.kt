// Kotlin supports  packages, whereas the package name does not have to be match the directory structure as in Java
package nagypalg.ktin120mins

import kotlinx.coroutines.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

// this is the entry point, such like in Java
// in Kotlin Arrays are objects, supporting proper generics (invariant)
fun main(args: Array<String>) {
    runDemos(args)
}

/*
Kotlin functions are declared with "fun". We can declare things globally,
no need to put everything into classes, like in Java.
Functions that do not return a value return the type Unit (like Java Void).
*/
fun basicsDemo(): Unit {
    // Kotlin does not have primitive types, everything is an object
    // but the compiler translates objects to primitive types whenever possible
    // we define mutable variables with "var"
    var mutableInt: Int = 12;
    log(mutableInt)
    mutableInt = 13
    log(mutableInt)
    // Kotlin does type inference whenever possible
    // Immutable variables (values) are defined with "val"
    val immutableInt = 24
    log(immutableInt)
    // This would be a compile error
    // immutableInt = 25

    // There are no automatic conversions between types (unlike in Java and like in Go)
    // So the following would be a compile error
    // val aLong: Long = mutableInt
    // There are toXXX methods for most of the usual conversions, which can be used instead
    val aLong: Long = mutableInt.toLong()
    log(aLong)

    // It is possible to have functions inside functions
    // Any? is Kotlin's java.lang.Object
    fun casts(x: Any?): Int {
        // Values can be casted with as
        // val anInt: Int = x as Int
        // But casting is dangerous and hardly ever needed
        // Note that if..else is an expression, so Kotlin does not have the ?..: ternary operator
        // Also note that Kotlin knows that x is an Int after we have checked
        return if (x is Int) x else 0
    }

    log(casts(mutableInt))

    /** Nothing is the type of a non-existent value, i.e. when we never return
     * Kotlin does not have checked exceptions, but they can be documented as part of the documentation comment
     * @throws Exception always
     */
    fun dieWithException(): Nothing {
        throw Exception("I am dead")
    }

    // try..catch is also an expression
    val result = try {
        dieWithException()
    } catch (e: Exception) {
        e.message
    }
    log(result)
}

// Type inference works for function return types, too, the Unit return type can be omitted
fun stringsDemo() {
    val aString = "Hello Kotlin"
    log(aString)
    // Kotlin has raw strings, they can be conveniently defined using trimMargin
    val aRawString = """Hello
        |\\ Kotlin \\
    """.trimMargin()
    log(aRawString)

    // String interpolation aka. string templates
    val msg = "The message is: '$aString'"
    log(msg)

    // Interpolation works also for raw strings!
    // We can insert the result of arbitrary computations
    val rawMsg = """The raw message is "${aString.uppercase()}""""
    log(rawMsg)

    // The only challenge with interpolation is how we can insert dollar in raw strings, but there are solutions
    val usd = "$"
    val rawWithDollar = """Printing dollars is tricky: $usd or ${'$'}"""
    log(rawWithDollar)

    val nonRawWithDollar = "In normal strings dollar can be escaped: 15\$"
    log(nonRawWithDollar)
}

// Default visibility in Kotlin is public (can be explicitly written, but not necessary)
// Other levels:
// internal - visible in the compilation unit / module (e.g. maven project)
// protected - only for classes, visible in inheritors
// private - visible only in the same file or in the same class
// Note that there is no "package private" level!

/**
 * This is a copy-paste of the Kotlin standard-library method joinToString and shows a lots of things.
 * - Arrays are objects
 * - Generics
 * - Extension methods
 * - Default arguments
 * - Shortcut notation and return type inference for one-liner functions
 * - KDoc supports markdown
 */
internal fun <T> Array<out T>.myJoinToString(
    separator: CharSequence = ", ",
    prefix: CharSequence = "",
    postfix: CharSequence = "",
    limit: Int = -1,
    truncated: CharSequence = "...",
    transform: ((T) -> CharSequence)? = null, // since 1.4 trailing commas are accepted
) = joinToString(separator, prefix, postfix, limit, truncated, transform)


// there are vararg parameters
private fun printStrings(vararg strings: String) {
    // Functions can be called with named arguments, so we can use the default values for any parameters
    log(strings.myJoinToString(prefix = "PrintStrings: ", postfix = "."), 1)
}

// One of the key features of Kotlin is the remediation of the "billion dollar mistake"
fun nullHandlingDemo() {
    // nullable strings are different type from non-nullable strings
    var nullableString: String? = null

    // the following line would be a compile error
    // println(nullableString.length)

    // Kotlin provides a null-safe operator. In this case nullableString?.length is null
    log(nullableString?.length)

    // Elvis operator ( ?: )for default values
    log(nullableString?.length ?: 0)

    // If we are entirely sure that a nullable variable cannot be null, we can use the !! operator
    // This may throw an NPE!
    // println(nullableString!!.length)

    // This would be a compile error
    // printStrings(nullableString)

    // Smart null handling: because we have checked for non-null, the compiler auto-converts String? to String
    if (nullableString != null) {
        println(nullableString.length)
        printStrings(nullableString)
    }

    nullableString = "not null"
    // Another example for smart null handling, the compiler knows that nullableString is actually a String now
    log(nullableString.length)
    printStrings(nullableString)

    // this has the type String and therefore we do not need the null-safe operators
    val nonNullString = "non null"
    log(nonNullString.length)
    printStrings(nonNullString)
    printStrings(nullableString, nonNullString)
}

// we can have type aliases
private typealias NickName = String

private typealias Name = String

fun typeAliasDemo() {

    // local functions possible
    fun greetWithNick(name: NickName) {
        log("Hey $name!", 2)
    }

    val pete = "Pete" // String
    val joe: NickName = "Joe"
    val john: Name = "John"

    // but type aliases do not create new types, i.e. we can call the method with String or with any alias of it
    // so it is rather for improving readability than for compile-time safety
    greetWithNick(pete)
    greetWithNick(joe)
    greetWithNick(john)

    // Note: "real" aliases called inline classes are coming,
    // but experimental at the moment

}

// collections

private fun printAll(c: Collection<Any>) {
    // { x -> doSomething(x)} is a lambda
    // If there is only one parameter for the lambda, we can use "it"

    // if the last parameter of a Kotlin function is a lambda, the parentheses can be omitted
    // This leads to much readable code and an easier creation of domain specific languages

    log(c.joinToString {

        // Kotlin has when which is much more powerful than Java's switch
        when (it) {

            // case conditions can be arbitrary
            // it is also an example for smart cast - we can use String methods without explicit cast
            // after checking it is a String
            is String -> "\"${it.uppercase()}\""
            is Number -> "=${it.toDouble()}"
            else -> it.toString()
        }
    }, 1)
}

// Generic type alias
typealias Predicate<T> = (T) -> Boolean

fun collectionsDemo() {

    // Kotlin has read-only sub-interfaces of Java collections and easy-to-use factory methods
    val langs = listOf("Kotlin", "Java", "Go", "Scala")
    // the read-only interfaces are covariant!
    printAll(langs)

    // but there are also mutable collections, of course (that are invariant)
    val mutableLangs = langs.toMutableList()
    mutableLangs.add("Python")
    // it works because the printAll method only uses the read-only, covariant interface
    printAll(mutableLangs)
    // the original immutable list is unchanged
    printAll(langs)

    val years = listOf(2016, 1996, 2012, 2004)
    printAll(years)

    // maps can be easily created with the X to Y notation which creates a pair
    // it is also an example of the infix notation: X to Y is the same as X.to(Y)
    val inceptionYears = mapOf("Kotlin".to(2016), "Java" to 1996, "Go" to 2012, "Scala" to 2004)
    // destructuring and iteration over collection with for .. in
    for ((lang, year) in inceptionYears) {
        log("$lang was created in $year")
    }
    printAll(inceptionYears.entries)

    // the usual FP constructs are supported
    // _ is used when we want to ignore something
    val newLangs = inceptionYears
        .filter { (_, y) -> y > 2010 }
        .map { (l, _) -> l.uppercase() }
        .sorted()

    printAll(newLangs)

    // Functions are first-class citizens: can be used as parameters and can be returned
    // Also, they can be declared locally in other functions
    fun <T> not(p: Predicate<T>): Predicate<T> {
        return fun(n: T) = !p.invoke(n)
        // !p(n) would also work
        // as well as return { !p(it)}
    }

    // Shortcut notation for functions, return can be omitted
    fun even(n: Int) = n % 2 == 0

    // functions can be referenced with ::
    fun odd(n: Int) = not(::even)(n)

    // sequences are like Java streams, they are lazily evaluated
    val naturalNums = generateSequence(0) { it + 1 }

    val oddNums = naturalNums.filter(::odd)

    val first5After100 = oddNums
        .filter { it > 100 }
        .take(5)
        .toList()
    printAll(first5After100)

}

// OOP

// Interfaces are like Java8 ones: they can have "default methods"
private interface Err {
    // these are properties, which are supported in Kotlin (unlike in Java)
    val msg: String
    val cause: Err?

    fun stackTrace(): List<Err> {
        return if (cause != null) {
            val result: MutableList<Err> = mutableListOf(this)
            // but smart null handling does not work here because the compiler cannot guarantee null-safety
            result.addAll(cause!!.stackTrace())
            result.toList()
        } else {
            listOf(this)
        }
    }

}

// Enums are supported
// In Kotlin there is no difference between "implements" and "overrides"
// Also, overrides must be explicit, it is not optional like in Java
// The primary constructor always comes after the class name
// If parameters of the primary constructor are also properties, this can be declared by using val/var
private enum class StandardErrors(override val msg: String) : Err {

    OOM("Out of Memory"), NPE("Null Pointer");

    override val cause: Err? = null
}

// Sealed classes are enums on steroids
// A sealed class can have subclasses, but all of them must be declared in the same file as the sealed class itself.

private sealed class SealedErr(override val msg: String, override val cause: Err? = null) : Err

private class SealedWithCode(val code: String) : SealedErr("Code: $code")

private class SpecialSealed : SealedErr("Special sealed")

private fun sealedErrToString(se: SealedErr) = when (se) {
    is SealedWithCode -> se.code
    is SpecialSealed -> se.msg
    // note: no default case is needed here
}

fun oopDemo() {

    // Except from interfaces and enums, classes can be locally declared in functions
    // primary constructor parameters can have default values
    abstract class AbsErr(override val msg: String, override val cause: Err? = null) : Err {

        // this is the body of the primary constructor (rarely needed)
        init {
            log("$msg: init")
        }

        // override must be explicit also for methods
        override fun toString(): String {
            var causeStr = ""
            // run is a "scope function", which sets this in the lambda
            cause?.run { causeStr = " cause: $msg" }
            return msg + causeStr
        }
    }

    // classes and methods are final by default, we have the explicitly open them for inheritance
    // abstract classes are open by default
    // when inheriting from classes, we have to specify the parameters of the parent constructor
    // this is like calling super in a Java constructor
    open class ErrWithCause(msg: String, cause: Err? = null) : AbsErr(msg, cause)

    class SimpleErr(msg: String) : ErrWithCause(msg) {

        // Secondary constructors are also possible
        constructor(cause: Err) : this("Caused by ${cause.msg}")
    }

    // there is no new keyword, class instantiation is like calling the constructor
    val simpleErr = SimpleErr("Simple")
    log(simpleErr)
    log(simpleErr.stackTrace())
    printSep()

    val cause = SimpleErr("Cause")
    val simpleCaused = SimpleErr(cause)
    log(simpleCaused)
    log(simpleCaused.stackTrace())
    printSep()

    val errWithCause = ErrWithCause("WithCause", cause)
    log(errWithCause)
    log(errWithCause.stackTrace())
    printSep()

    log(StandardErrors.OOM)

    printSep()

    val sealedWithCode = SealedWithCode("ABC")
    log("Sealed with code, code=${sealedWithCode.code}, message=${sealedWithCode.msg}")
    log(sealedErrToString(SpecialSealed()))

}


fun dataClassDemo() {

    // A data class has all kinds of goodies, like automatic toString, equals, hashCode
    // support for destructuring and copying
    data class Config(
        val host: String = "localhost",
        val port: Int = 8080,
        val baseUrl: String = "/"
    )

    // Note how default constructor arguments and named arguments make builders unnecessary in most cases
    val cfg1 = Config()
    val cfg2 = Config("localhost")
    val cfg3 = Config(port = 8080, host = "localhost")
    val cfg4 = cfg1.copy(baseUrl = "/kotlin")

    log(cfg1)
    // == is like equals in Java (value equality)
    log("cfg1 == cfg2, i.e. $cfg1 == $cfg2 : ${cfg1 == cfg2}")
    // === is like == in Java (same object)
    log("cfg1 === cfg2, i.e. $cfg1 === $cfg2 : ${cfg1 === cfg2}")
    log("cfg1 == cfg3, i.e. $cfg1 == $cfg3 : ${cfg1 == cfg3}")
    log("cfg1 == cfg4, i.e. $cfg1 == $cfg4 : ${cfg1 == cfg4}")

    val (host, port, _) = cfg4
    log("cfg4 host=$host, port=$port")

    fun makeConfig(host: String) = Config(host = host)

    val (host2, port2, baseUrl2) = makeConfig("myhost")
    log(
        """makeConfig("myhost")
        |host=$host2,
        |port=$port2,
        |baseUrl=$baseUrl2""".trimMargin()
    )

}

fun genericsDemo() {

    // Kotlin support declaration-site generics in addition to Java's use-site generics
    // This means that in most cases we do not have to deal with upper and lower bounds
    // out T means that this type only produces objects of type T but does not consumes them
    // in this case this class is covariant
    // in T is also supported for contravariant types
    // simply T denotes an invariant type
    // upper and lower bounds are also supported using the usual Kotlin notation
    // It is highly recommended to read the Kotlin reference: https://kotlinlang.org/docs/reference/generics.html
    // It is also a very useful resource to finally understand generics in Java
    open class NumberBox<out T : Number>(value: T) {
        private var _value: T = value

        open fun get() = _value
    }

    // Note that this class must be invariant because it both produces and consumes T
    class MutableNumberBox<T : Number>(value: T) : NumberBox<T>(value) {
        private var _value: T = value

        override fun get() = _value
        fun set(value: T) {
            _value = value
        }
    }

    fun printBoxValue(box: NumberBox<Number>) {
        log("Box value: ${box.get()}", 2)
    }

    val b1 = NumberBox(123.456)
    printBoxValue(b1)

    val b2 = NumberBox(12)
    printBoxValue(b2)

    val b3: MutableNumberBox<Number> = MutableNumberBox(34L)
    printBoxValue(b3)
    b3.set(23.45)
    printBoxValue(b3)

    // this would be a compile error
    // val b4: MutableNumberBox<Long> = b3

    // and this also
    // val b5 = NumberBox("abc")

}

//no statics

private fun deCapitalizeFirstChar(s: String): String {
    // note: we do not have to check for null because of the type system
    if (s.isEmpty()) {
        return s
    }
    val firstChar = s[0]
    return firstChar.lowercaseChar() + s.substring(1)
}

// Kotlin does not have the concept of Java statics, but provides different, arguably better alternatives

// support for singletons, which are "normal" instances i.e. can implement interfaces and inherit from classes
private object MyStringUtils {

    // For Java interop, we can have the Kotlin compiler generate a static method, too
    @JvmStatic
    fun deCapFirst(s: String) = deCapitalizeFirstChar(s)
}

// another, more idiomatic alternative is the usage of extension functions
// even system types can be extended!
private fun String.deCapFirst() = deCapitalizeFirstChar(this)

private class MyClass(private val txt: String) {

    private fun deCapFirst() = deCapitalizeFirstChar(txt)

    // a companion object is invoked with the name of the enclosing class
    companion object {
        fun deCapFirst(txt: String) = MyClass(txt).deCapFirst()
    }
}

fun noStaticsDemo() {
    val s = "Decapitalize Me"
    log(MyStringUtils.deCapFirst(s))
    // arguably this is the cleanest and most readable solution
    log(s.deCapFirst())
    log(MyClass.deCapFirst(s))
}

/*
This will demo lambdas with receivers, i.e. when "this" is set to an object inside the lambda
 */
fun lambdaWithReceiverDemo() {

    data class Config(var host: String = "localhost", var port: Int = 8080, var baseUrl: String = "/") {
        // the apply scope function sets this and returns the same object. It is great for builder functions.
        fun host(h: String) = apply { this.host = h }

        fun port(p: Int) = apply { this.port = p }
        fun baseUrl(u: String) = apply { this.baseUrl = u }

        // infix functions can be invoked without parentheses
        // i.e. instead of "a.m(b)" we can (but don't have to!) write "a m b"
        infix fun portPlus(delta: Int) = apply { this.port += delta }
    }

    // this receives a "normal" lambda
    fun withConfig(cfg: Config, logic: (Config) -> Unit) {
        logic(cfg)
    }

    // this uses a lambda with a receiver: (Receiver).(params) -> Return
    fun Config.doWithConfig(logic: (Config).() -> Config) = logic(this)

    val cfg = Config()

    // no receiver, we use "it"
    withConfig(cfg) { log(it.host, 4) }

    // with receiver, "this" refers to cfg
    cfg.doWithConfig {
        log(this, 4)
        this
    }

    (cfg.apply {
        host = "srv1"
        port = 80
        baseUrl = "/kotlin"
    } portPlus 2)
        // "also" returns the object it is applied on, but does not set this
        // it is great for additional logging
        .also { log(it) }

    fun printNonNullConfig(cfg: Config?) {
        // Another scope function, it does not set this and returns the result of the lambda
        // it is frequently used instead of "if (x != null)"
        cfg?.let { log("Config: $it", 2) }
    }

    val nullCfg: Config? = null

    printNonNullConfig(nullCfg)
    printNonNullConfig(cfg)

    val cfg2 = Config().host("srv2").port(83).baseUrl("/builder")
        .also { log("With builder: $it") }
    printNonNullConfig(cfg2)

    printNonNullConfig(Config(host = "srv3", port = 84, baseUrl = "/namedargs"))

    // note that extension functions can be declared locally (i.e. they are valid only in the current function)
    // Inside the extension function the current object which is extended is visible as "this"
    fun String.firstChar() = this.first()
    log("""First char of "first": "${"first".firstChar()}"""")

}

// operator overloading

private class Counter(initialValue: Int) {

    var value: Int = initialValue
        private set // we just change the visibility of the setter, but keep the default implementation
        // but we can also write our own getter/setter. "field" refers to the backing field of the property
        get() = field.also { log("Counter's field was accessed, its value=$field", 3) }

    // overload Counter++ and ++Counter
    // Kotlin defines a handful of standard method names that can overload some standard operators
    // but it is NOT possible to create all kind of crazy new operators
    operator fun inc() = Counter(value + 1)

    // overload Counter instance invocation
    operator fun invoke() = value

}

fun operatorOverloadingDemo() {
    var counter1 = Counter(0)
    var counter2 = Counter(5)

    log(counter1())
    counter1++
    // the method version is still usable
    counter1.inc()
    log(counter1())
    log(counter2())
    ++counter2
    log(counter2())
}

fun delegationDemo() {
    // interfaces can be implemented by delegation (also multiple!)
    // this is similar to "mixins" or "traits" in other languages
    // delegation is indicated by the "by" keyword
    class SummingList<out T : Number>(private val delegateList: List<T>) : List<T> by delegateList {
        fun sum(): Double = delegateList.sumOf { it.toDouble() }

        // toString is not part of the List interface
        override fun toString(): String {
            return delegateList.toString()
        }
    }

    val myIntList = SummingList(listOf(2, 3, 4))
    log("Sum of $myIntList is ${myIntList.sum()}")

    val myDoubleList = SummingList(listOf(3.0, 4.0, 5.5))
    log("Sum of $myDoubleList is ${myDoubleList.sum()}")
}

//propertiesInMapDemo

//enums cannot be local in functions
private enum class Sex {
    MALE, FEMALE
}


fun propertiesInMapDemo() {

    // delegation works also for properties
    // there are other standard delegators, like lazy or observable
    // and user-specific delegators are also possible
    class User(val map: Map<String, Any>) {
        val name: String by map
        val age: Int by map
        val sex: Sex by map
    }

    val user = User(
        mapOf(
            "name" to "John Smith",
            "age" to 25,
            "sex" to Sex.MALE
        )
    )

    log(user.map)
    log(user.name)
    log(user.age)
    log(user.sex)

}

// coroutines

private fun logThread(msg: String) = println("coroutinesDemo [${Thread.currentThread().name}] $msg")

// (Part of) coroutines became stable in the newest Kotlin version
@SinceKotlin("1.3")
fun coroutinesDemo() {

    // coroutines do not necessarily run in a separate thread
    // but their execution can be suspended without blocking the thread
    // if a function uses any suspending function, it must be marked as such
    suspend fun incrementAndLog(counter: AtomicInteger, dispatcher: String) {
        logThread("$dispatcher before delay")
        // this will suspend the execution for 100ms without blocking the thread
        delay(100L)
        counter.incrementAndGet()
        logThread("$dispatcher after delay")
    }

    val counter = AtomicInteger()

    // coroutines must be started in a scope
    // runBlocking starts a scope that blocks the current thread until the scope finishes
    runBlocking {

        // one way to count in for - Kotlin does not have the classic, C-style for cycle, "only" the for .. in variant
        // this creates a range [0,5), i.e. it is open at the end
        for (i in 0 until 5) {
            // this launches a coroutine with the default settings, which means
            // run in current thread and start immediately
            launch {
                incrementAndLog(counter, "parent $i")
            }
        }


        // another way to count with for using a closed range [1,5]
        for (i in 1..5) {
            // this will run using a predefined thread pool and will not start immediately
            val job = launch(Dispatchers.Default, CoroutineStart.LAZY) {
                incrementAndLog(counter, "Dispatchers.Default $i")
            }
            // launch returns a job handle which can be used to start it,
            // but also for other things: cancel or wait until it completes
            job.start()
        }

        // this is a nicer way to repeat something 5 times than using a for cycle
        repeat(5) {
            // this will use the current thread until the first suspending function, when it switches to another thread
            // works only if the coroutine does not use any thread local variables
            launch(context = Dispatchers.Unconfined) {
                incrementAndLog(counter, "Dispatchers.Unconfined $it")
            }
        }

        // coroutines are very lightweight, it is not an issue to start many of them
        // this would be clearly impossible with threads
        repeat(12_000) {
            launch {
                delay(1000L)
                counter.incrementAndGet()
            }
        }
    }

    val latch = CountDownLatch(1)

    // this will start a coroutine in a separate, own thread named "test"
    thread(name = "test") {
        runBlocking {
            repeat(5) {
                launch {
                    incrementAndLog(counter, "own thread $it")
                }
            }
        }
        latch.countDown()
    }

    latch.await()
    log("\nCounter = ${counter.get()}\n")

    runBlocking {

        val time = measureTimeMillis {
            // if the coroutine should produce a result, async/await can be used
            // which should be familiar from JavaScript
            // launch is like Runnable, async is like Callable in Java
            // otherwise async has the same options as launch
            val lazy = async(start = CoroutineStart.LAZY, context = Dispatchers.Default) { logAndReturn(100) }
            val eager = async { logAndReturn(200) }
            lazy.start() // start the first one
            logThread("The answer is ${lazy.await() + eager.await()}")
        }
        logThread("Completed in $time ms")

    }

}

// note that we simply return an Int, it becomes a Deferred<Int> automatically when invoked from async
private suspend fun logAndReturn(value: Int): Int {
    delay(100L)
    logThread(value.toString())
    return value
}

// DSL

// there are (compile time) constants, too
private const val INDENT = "  "

// with this we allow using only the latest receiver in the lambda
@DslMarker

// this is how annotations are defined
annotation class ListItemDslMarker

@ListItemDslMarker
private interface ListItem {

    fun renderMarkdown(builder: StringBuilder = StringBuilder(), indent: String = ""): String

    fun renderHtml(builder: StringBuilder = StringBuilder(), indent: String = ""): String

}

private class SimpleListItem(val text: String) : ListItem {
    override fun renderHtml(builder: StringBuilder, indent: String): String {
        builder.append("$indent<li>$text</li>\n")
        return builder.toString()
    }

    override fun renderMarkdown(builder: StringBuilder, indent: String): String {
        builder.append("$indent* $text\n")
        return builder.toString()
    }

}

private class ComplexListItem(val level: Int = 0) : ListItem {

    val children: MutableList<ListItem> = mutableListOf()

    override fun renderMarkdown(builder: StringBuilder, indent: String): String {
        val childIndent = INDENT.repeat(level)
        for (child in children) {
            child.renderMarkdown(builder, childIndent)
        }
        return builder.toString()
    }

    override fun renderHtml(builder: StringBuilder, indent: String): String {
        builder.append("$indent<ul>\n")
        val childIndent = INDENT.repeat(level + 1)
        for (child in this.children) {
            child.renderHtml(builder, childIndent)
        }
        builder.append("$indent</ul>\n")
        return builder.toString()
    }

    // using extension functions and lambdas with receivers it is really easy to create your own DSL
    // Actually most of Kotlin's great functionality is not built-in to the language
    // but implemented using functions that use lambdas with (or without) receivers
    operator fun String.unaryMinus() {
        children.add(SimpleListItem(this))
    }

    fun list(init: ComplexListItem.() -> Unit): ComplexListItem {
        val item = ComplexListItem(level + 1)
        children.add(item)
        init(item)
        return item
    }

}

private fun list(init: ComplexListItem.() -> Unit): ComplexListItem {
    val item = ComplexListItem()
    init(item)
    return item
}

fun dslDemo() {

    // using our new "list DSL"
    val myList = list {
        -"item 1"
        -"item 2"
        list {
            -" sub item 1"
            list {
                -"sub sub item 1"
                -"sub sub item 2"
                -"sub sub item 3"
                -"sub sub item 4"
            }
            -" sub item 2"
        }
        -"item 3"
    }

    log(myList.renderMarkdown())
    log(myList.renderHtml())

}

//////////////////////////////////////////////
// internal functions supporting demo running
//////////////////////////////////////////////

private typealias Demo = () -> Unit

private fun executeDemo(name: String, d: Demo) {
    printDemoSep(name)
    d()
}

private fun printDemoSep(name: String) {
    val sep = makeSep(sepChar = '#')
    val begin = """
        |
        |$sep
        |$name
        |$sep
        |
    """.trimMargin()
    println(begin)
}

// default values
private fun printSep(numChars: Int = 80, sepChar: Char = '-') {
    println(makeSep(numChars, sepChar))
}

// extension functions on built-in types!
private fun makeSep(numChars: Int = 80, sepChar: Char = '-') = sepChar.toString().repeat(numChars)

private fun log(msg: Any?, callerDepth: Int = 1) {
    val stacktrace = Thread.currentThread().stackTrace
    val caller: String = stacktrace[callerDepth + 2].methodName
    val thread: String = Thread.currentThread().name
    println("$caller [$thread] $msg")

}

private fun runDemos(args: Array<String>) {
    // special collection subclasses can be created, too
    // here we create a LinkedHashMap to keep insertion order
    val demos = linkedMapOf(
        "basics" to ::basicsDemo,
        "strings" to ::stringsDemo,
        "null handling" to ::nullHandlingDemo,
        "type alias" to ::typeAliasDemo,
        "collections" to ::collectionsDemo,
        "oop" to ::oopDemo,
        "data class" to ::dataClassDemo,
        "generics" to ::genericsDemo,
        "no statics" to ::noStaticsDemo,
        "lambda with receiver" to ::lambdaWithReceiverDemo,
        "operator overloading" to ::operatorOverloadingDemo,
        "delegation" to ::delegationDemo,
        "property delegation to map" to ::propertiesInMapDemo,
        "coroutines" to ::coroutinesDemo,
        "DSL" to ::dslDemo
    )
    val pattern = if (args.isNotEmpty()) args[0] else ".*"
    println("Read pattern '$pattern'")
    // There are slight improvements in regexp handling, too
    val execRegexp = pattern.toRegex(RegexOption.IGNORE_CASE)
    val demosToExec = demos.filter { execRegexp.containsMatchIn(it.key) }
    println("Will execute ${demosToExec.size} demo${if (demosToExec.size == 1) "" else "s"}.")
    for ((name, demo) in demosToExec) {
        executeDemo(name, demo)
    }
}
