// Kotlin supports  packages, whereas the package name does not have to be match the directory structure as in Java
package nagypalg.ktin30mins

import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main() {
    printSep("Type inference")
    typeInference()

    printSep("Strings")
    strings()

    printSep("Extension functions")
    extensionFunctions()

    printSep("Null handling")
    nullHandling()

    printSep("Collections")
    collections()

    printSep("Higher order functions and sequences")
    higherOrderFunctionsAndSequences()

    printSep("Object properties and scope functions")
    propertiesAndScopeFunctions()

    printSep("Data classes")
    dataClasses()

    printSep("Ranges")
    ranges()

    printSep("Coroutines")
    coroutines()

}

// note: if you open this file in IntelliJ, you will find some warnings
// this is on purpose to make some Kotlin concepts easier to understand for Java developers
fun typeInference(): Unit {
    // no primitive types, mutable variables
    var mutableInt: Int = 12;

    // type and semicolon inference, immutable values
    val immutableInt = 240

    // This would be a compile error
    // immutableInt = 250

    // local functions
    // Any? is Kotlin's Object
    // shortcut notation for one-liner functions
    // when is Kotlin's switch, just more powerful, supports arbitrary case conditions, and it is an expression
    fun typeString(x: Any?): String = when (x) {
        // smart casts, if..else is also an expression
        is Int -> (if (x > 100) "Big" else "Small") + " integer number"
        is String -> "String of " + x.length + " chars"
        else -> "Unknown"
    }

    fun printWithType(x: Any?) = println(x.toString() + ": " + typeString(x))

    printWithType(mutableInt)
    mutableInt = 130
    printWithType(mutableInt)

    printWithType(immutableInt)

    printWithType("Kotlin")
    printWithType(true)
}

fun strings() {
    val aString = "Hello Kotlin"
    println(aString)

    // Kotlin has raw strings, they can be conveniently defined using trimMargin
    val aRawString = """Hello
        |\\ Kotlin \\
    """.trimMargin()
    println(aRawString)

    // String interpolation aka. string templates
    val msg = "The message is: '$aString'"
    println(msg)

    // Interpolation works also for raw strings!
    // We can insert the result of arbitrary computations
    val rawMsg = """The raw message is "${aString.uppercase(Locale.getDefault())}""""
    println(rawMsg)
}

// default parameters, extension functions (repeat)
private fun makeSep(numChars: Int = 80, sepChar: Char = '-') = sepChar.toString().repeat(numChars)

private fun printSep(demo: String) {
    // invocation with default parameters
    val sep = makeSep()
    println("\n$sep\n$demo\n$sep")
}

// there are vararg parameters
private fun printStrings(vararg strings: String) {
    // Functions can be called with named arguments, so we can use the default values for any parameters
    println(strings.joinToString(prefix = "PrintStrings: ", postfix = "."))
}

// extension functions
// even system types can be extended!
private fun String.deCapFirst(): String {
    // this is the receiver string
    if (isEmpty()) {
        return this
    }
    val firstChar = this[0]
    return firstChar.lowercaseChar() + substring(1)
}

fun extensionFunctions() {
    val s = "Decapitalize Me"
    println(s.deCapFirst())
}


// One of the key features of Kotlin is the remediation of the "billion dollar mistake"
fun nullHandling() {
    // nullable strings are different type from non-nullable strings
    var nullableString: String? = null

    // the following line would be a compile error
    // println(nullableString.length)

    // Kotlin provides a null-safe operator. In this case nullableString?.length is null
    println(nullableString?.length)

    // Elvis operator ( ?: )for default values
    println(nullableString?.length ?: 0)

    // This would be a compile error
    // printStrings(nullableString)

    fun printNullableString(s: String?) {
        // Smart null handling: because we have checked for non-null, the compiler auto-converts String? to String
        if (s != null) {
            printStrings(s)
            // this has the type String and therefore we do not need the null-safe operators
            val nonNullString = "non null"
            println(nonNullString.length)
            //varargs
            printStrings(s, nonNullString)
        }
    }

    nullableString = "something"
    printNullableString(nullableString)
}

// collections and lambdas

private fun printAll(c: Collection<Any>) {
    // { x -> doSomething(x)} is a lambda
    // If there is only one parameter for the lambda, we can use "it"

    // if the last parameter of a Kotlin function is a lambda, the parentheses can be omitted
    // This leads to much readable code and an easier creation of domain specific languages

    println(
        c.joinToString {
            it.toString().uppercase() // note: it is better to specify the locale!
        }
    )
}

fun collections() {

    // Kotlin has read-only sub-interfaces of Java collections and easy-to-use factory methods
    val langs = listOf("Kotlin", "Java", "Go", "Scala")
    // the read-only interfaces are covariant!
    printAll(langs)

    // but there are also mutable collections, of course (that are invariant)
    val mutableLangs = langs.toMutableList()
    mutableLangs.add("Python")
    // it works because the printAll method only uses the read-only, covariant interface
    printAll(mutableLangs)
    // the original immutable list is unchanged
    printAll(langs)

    val years = listOf(2016, 1996, 2012, 2004)
    printAll(years)

    // maps can be easily created with the X to Y notation which creates a pair
    // this example also demonstrates infix functions
    val inceptionYears = mapOf("Kotlin".to(2016), "Java" to 1996, "Go" to 2012, "Scala" to 2004)
    // destructuring and iteration over collection with for .. in
    for ((lang, year) in inceptionYears) {
        println("$lang was created in $year")
    }
    printAll(inceptionYears.entries)

    // the usual FP constructs are supported
    // _ is used when we want to ignore something
    val newLangs = inceptionYears
        .filter { (_, y) -> y > 2010 }
        .map { (l, _) -> l.uppercase(Locale.getDefault()) }
        .sorted()

    printAll(newLangs)
}


// Generic type alias
typealias Predicate<T> = (T) -> Boolean

fun higherOrderFunctionsAndSequences() {

    // Functions are first-class citizens: can be used as parameters and can be returned
    // Also, they can be declared locally in other functions
    fun <T> not(p: Predicate<T>): Predicate<T> = { !p(it) }

    fun even(n: Int) = n % 2 == 0

    // functions can be referenced with ::
    fun odd(n: Int) = not(::even)(n)

    // sequences are like Java streams, they are lazily evaluated
    // and work with older JVMs, Kotlin/JS and Kotlin/Native, as well
    val naturalNums = generateSequence(0) { it + 1 }

    val oddNums = naturalNums.filter(::odd)

    val first5After100 = oddNums
        .filter { it > 100 }
        .take(5)
        .toList()
    printAll(first5After100)

}

fun propertiesAndScopeFunctions() {
    // class in a function!
    class Frame {
        var width: Int = 800
        var height: Int = 600

        val pixels: Int
            get() = width * height // custom getter
    }

    fun printFrameInfo(f: Frame) = println("A frame with ${f.width}x${f.height} has ${f.pixels} pixels")

    val f = Frame()
    printFrameInfo(f)

    fun changeAndPrintFrame(f: Frame?, w: Int = 800, h: Int = 600) = f?.let {
        // let sets it to f and will return the last value
        it.apply {
            // apply sets this and will return this
            width = w
            height = h
        }.also {
            // also sets it and will return it
            printFrameInfo(it)
        }
    }

    changeAndPrintFrame(f, 1024, 768)
    changeAndPrintFrame(f)
    changeAndPrintFrame(null)
}


fun dataClasses() {

    // A data class has all kinds of goodies, like automatic toString, equals, hashCode
    // support for destructuring and copying
    data class Config(
        val host: String = "localhost",
        val port: Int = 8080,
        val baseUrl: String = "/"
    )

    // Note how default constructor arguments and named arguments make builders unnecessary in most cases
    val cfg1 = Config()
    val cfg2 = Config(port = 8080, host = "localhost")
    val cfg3 = cfg1.copy(baseUrl = "/kotlin")
    println(cfg1)
    println(cfg2)
    println(cfg3)

    // == is like equals in Java (value equality)
    println("cfg1 == cfg2, i.e. $cfg1 == $cfg2 : ${cfg1 == cfg2}")
    // === is like == in Java (referential equality)
    println("cfg1 === cfg2, i.e. $cfg1 === $cfg2 : ${cfg1 === cfg2}")

    // destructuring
    val (_, port, baseUrl) = cfg3
    println("cfg3 port=$port, baseUrl='$baseUrl'")

}

fun ranges() {
    fun processRange(r: Iterable<Int>) {
        val rangeResults = mutableListOf<Int>()
        for (i in r) {
            rangeResults.add(i)
        }
        println("$r: ${rangeResults.joinToString { it.toString() }}")
    }

    processRange(1..10)
    processRange(0 until 10)
    processRange(2..10 step 2)
    processRange(10 downTo 1 step 3)

    if (5 in 1..10) {
        println("5 is between 1 and 10")
    }
}

// (Part of) coroutines became stable in the newest Kotlin version
@SinceKotlin("1.3")
fun coroutines() {

    val counter = AtomicInteger()

    // coroutines must be started in a scope
    // runBlocking starts a scope that blocks the current thread until the scope finishes
    runBlocking {

        // coroutines are very lightweight, it is not an issue to start many of them
        // this would be clearly impossible with threads
        repeat(12_000) {
            launch {
                delay(1000L)
                counter.incrementAndGet()
            }
        }
    }
    println("\nCounter = ${counter.get()}\n")

    val latch = CountDownLatch(1)

    // this will start a coroutine in a separate, own thread
    thread(name = "test") {
        runBlocking {
            repeat(5) {
                launch {
                    counter.incrementAndGet()
                }
            }
        }
        latch.countDown()
    }

    latch.await()
    println("\nCounter = ${counter.get()}\n")

    runBlocking {

        val time = measureTimeMillis {
            // if the coroutine should produce a result, async/await can be used
            // which should be familiar from JavaScript
            // launch is like Runnable, async is like Callable in Java
            // otherwise async has the same options as launch
            val lazy = async(start = CoroutineStart.LAZY) { delayAndReturn(100) }
            val eager = async { delayAndReturn(200) }
            lazy.start() // start the first one
            println("The answer is ${lazy.await() + eager.await()}")
        }
        println("Completed in $time ms")

    }

}

// note that we simply return an Int, it becomes a Deferred<Int> automatically when invoked from async
private suspend fun delayAndReturn(value: Int): Int {
    delay(100L)
    return value
}
